#!/usr/bin/env python

import sys
import grequests 
import requests
from bs4 import BeautifulSoup
from prettytable import PrettyTable
from progressbar import ProgressBar
import re

from util.googlesearch import gSearch
from util.output import write_outfile
from util.output import write_dbfile

class BlistData:
    def __init__(self, name, outfile, proxy, dbflag):
        self.comp_name = name
        self.glist = [] 
        self.outfile = outfile
        self.proxies = {}
        if proxy != None:
            self.proxies['http'] = 'http://' + proxy
            self.proxies['https'] = 'https://' + proxy
        self.dbflag = dbflag
        self.headers = { \
            'Host': 'www.business-lists.com', \
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0', \
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8', \
            'Accept-Language': 'en-US,en;q=0.5', \
            'Accept-Encoding': 'gzip, deflate', \
            'Connection': 'keep-alive'
            }
        
    # make both the google and blist request / process results
    def get_master_request(self, configDict):
        blist_urls = ["www.listsite.net/search/detail.asp","www.business-lists.com/search/detail.asp","www.list-brokers.com/search/detail.asp"]
        s = requests.Session()
        choice = -1
        choices = set([])
        tmp_list = []
        ready = False
        blist_search = gSearch(self.comp_name, configDict, blist_urls, self.proxies)
        blist_search.get_company()
        self.glist = blist_search.glist
        print "[+] For multiple URL selections enter one at a time. Scraping will start if you choose to enter your own URL or by pressing <ENTER> without any input. Or search again, I don't care."
        while not ready:
            try:
                choice = int(raw_input("Select a URL to scrape: "))
                if choice > (len(self.glist)+1) or choice < 0:
                    print "[-] Invalid selection. Value has to be between %d and %d" % (0, len(self.glist)+1)
                elif int(choice) == len(self.glist):
                    entered_url = raw_input("Enter a business-lists.com URL: ")
                    self.glist.append({'count': choice, 'company': u'Enter your own URL', 'link': entered_url})
                    choices.add(choice)
                    ready = True
                elif int(choice) == len(self.glist)+1:
                    print "[+] Searching again..."
                    name = raw_input("Enter company name to search: ")
                    blist_search = gSearch(name, configDict, blist_urls, self.proxies)
                    blist_search.get_company()
                    self.glist = blist_search.glist
                else:
                    choices.add(choice)
            except ValueError:
                if len(choices) == 0:
                    print "[-] No URLs selected yet."
                else:
                    ready = True
            
        for c in choices:
            targeturl = [ result['link'] for result in self.glist if result['count'] is c ]
            if len(self.proxies) == 0:
                #HEAD request to set cookies
                s.head(targeturl[0], headers=self.headers, verify=False)
                page = s.get(targeturl[0], headers=self.headers, verify=False)
            else:
                requests.packages.urllib3.disable_warnings() #disable SSL warning when using proxies
                #HEAD request to set cookies
                s.head(targeturl[0], headers=self.headers, proxies=self.proxies, verify=False)
                page = s.get(targeturl[0], headers=self.headers, proxies=self.proxies, verify=False)

            url = targeturl[0].find("detail.asp")
            tmp_list.append(self.get_num_employees(page, targeturl[0][0:url]))
            new_list = list(set(reduce(lambda x,y: x+y,tmp_list)))
        
        print "%s%d%s" % ("\n[+] Scraping ", len(choices)," URL(s)")
        emp_list = self.set_concurrency(new_list, s)
        self.display_emp_data(emp_list)
        if self.dbflag:
            write_dbfile(emp_list, self.outfile, "business-lists.com")
        else:
            write_outfile(emp_list, self.outfile, "business-lists.com")
        return emp_list

    def get_num_employees(self, page, url):
        employees = []
        soup = BeautifulSoup(page.text, 'html.parser')
        for row in soup.find_all(href=re.compile("detail2")):
            employees.append(url+row['href'])
        return employees
        
    def set_concurrency(self, employees, sess):
        emp_list = []
        concurrent = -1
        print "\n[+] %d employee record(s) found\n" % (len(employees))
        print "[+] The default is 1 request at a time."
        while int(concurrent) > 10 or int(concurrent) <= 0:
            concurrent = raw_input("Enter the number of simultaneous requests, max of 10. <ENTER> for default: ")
            try:            
                concurrent = int(concurrent)
                if concurrent <= 10 and concurrent > 0:
                    print "%s%d%s" % ("[+] Using the value of ", concurrent, " request(s)...Get a cup of coffee...This can take a minute")
            except:
                print "[+] Using default value of 1 request...Get a cup of coffee...This can take a minute"
                concurrent = 1
        pbar = ProgressBar()
        for i in pbar(range(0, len(employees), concurrent)):
            record = self.get_blist_request(employees[i:i+concurrent], sess)
            emp_list.extend(record)
        return emp_list

    # send simultaneous requests
    def get_blist_request(self, urls, sess):
        if len(self.proxies) == 0:
            rs = (grequests.get(u, session=sess, headers=self.headers, verify=False) for u in urls)
        else:
            requests.packages.urllib3.disable_warnings() #disable SSL warning when using proxies
            rs = (grequests.get(u, session=sess, headers=self.headers, proxies=self.proxies, verify=False) for u in urls)

        results = grequests.map(rs)
        emp_records = []
        for r in results:
           emp_records.append(self.employee(r))
        return emp_records

    # extract contact info for each employee record
    def employee(self, emp_page):
        emp_dict = {}
        soup = BeautifulSoup(emp_page.text, 'html.parser')
        record = soup.find("h3") #list-brokers.com
        if record is None:
            record = soup.find("strong") #business-lists.com
        name = soup.find(itemprop="name")
        title = soup.find(itemprop="title")
        phone = soup.find(itemprop="telephone")
        email = soup.find(itemprop="email")
        name_info = self.check_contact_info(name)
        title_info = self.check_contact_info(title)
        phone_info = self.check_contact_info(phone)
        email_info = self.check_contact_info(email)
        try:
            contact_info = str(record.br.contents[1])
            direct_phone_location = contact_info.find("Direct Phone:")
            if direct_phone_location != -1:
                direct_phone_info = contact_info[direct_phone_location+14:direct_phone_location+26]
            else:
                direct_phone_info = "Not Found"
        except:
            print "[-] Error finding direct phone information for %s" % (name)
            print "[-] Try running the tool again if few results were found"
            direct_phone_info = "Not Found"
        emp_dict['Employee'] = name_info
        emp_dict['Role'] = title_info
        emp_dict['Generic Phone'] = phone_info
        emp_dict['Direct Phone'] = direct_phone_info
        emp_dict['Email Address'] = email_info
        emp_dict['City'] = "Not Found"
        emp_dict['State'] = "Not Found"
        return emp_dict
        
    # check if contact item exists
    def check_contact_info(self, item):
        if item is None:
            return "Not found"
        else:
            return item.string
            
    def display_emp_data(self, emp_list):
        count = 0
        theader = PrettyTable([
            "Id", 
            "Employee", 
            "Role",
            "Email Address",
            "Generic Phone",
            "Direct Phone"
            ])
        theader.align["Id"] = "l"
        theader.align["Employee"] = "l"
        theader.align["Role"] = "l"
        theader.align["Email Address"] = "l"
        theader.align["Generic Phone"] = "l"
        theader.align["Direct Phone"] = "l"
        theader.padding_width = 1
        for result in emp_list:
            theader.add_row([
                "[" + str(count) + "]", 
                result['Employee'], 
                result['Role'],
                result['Email Address'],
                result['Generic Phone'],
                result['Direct Phone']
                ])
            count += 1
        try:
            print theader
        except RuntimeError:
            print "[-] Error printing employee data, will save to file."

    def get_multi_url_request(self, infile):        
        s = requests.session()
        count = 0
        tmp_list = []
        theader = PrettyTable(["Id", "Target URL"])
        theader.align["Id"] = "l"
        theader.align["Target URL"] = "l"
        theader.padding_width = 1
        for targeturl in infile:
            theader.add_row([
                "[" + str(count) + "]", 
                targeturl.strip()
                ]) 
            if len(self.proxies) == 0:
                #HEAD request to set cookies
                s.head(targeturl, headers=self.headers, verify=False)
                page = s.get(targeturl, headers=self.headers, verify=False)
            else:
                requests.packages.urllib3.disable_warnings() #disable SSL warning when using proxies
                #HEAD request to set cookies
                s.head(targeturl, headers=self.headers, proxies=self.proxies, verify=False)
                page = s.get(targeturl, headers=self.headers, proxies=self.proxies, verify=False)
            url = targeturl.find("detail.asp")    
            tmp_list.append(self.get_num_employees(page, targeturl[0:url]))
            new_list = list(set(reduce(lambda x,y: x+y,tmp_list)))
            count += 1
        print theader
        print "%s%d%s" % ("[+] ", count," URLs loaded for testing")
        emp_list = self.set_concurrency(new_list, s)
        self.display_emp_data(emp_list)
        if self.dbflag:
            write_dbfile(emp_list, self.outfile, "business-lists.com")
        else:
            write_outfile(emp_list, self.outfile, "business-lists.com")
        return emp_list
