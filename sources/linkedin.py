#!/usr/bin/env python

import sys
import json
import oauth2 as oauth
import grequests
import httplib2
from prettytable import PrettyTable

from util.output import write_outfile
from util.output import write_dbfile

class LinkinLog:
    def __init__(self, name, outfile, configDict, domain, proxy, dbflag):
        self.comp_name = name
        self.outfile = outfile
        self.consumer_key = configDict['consumer_key']
        self.consumer_secret = configDict['consumer_secret']
        self.user_token = configDict['user_token']
        self.user_secret = configDict['user_secret']
        self.domain = domain[1:len(domain)]
        self.coId = ""
        self.throttle = 250 #API throttle limit
        self.proxies = {}
        if proxy != None:
            self.proxies['http'] = 'http://' + proxy
            self.proxies['https'] = 'https://' + proxy 
        self.dbflag = dbflag

    def oauthSetter(self):  
        # Use your API key and secret to instantiate consumer object
        consumer = oauth.Consumer(self.consumer_key, self.consumer_secret)
         
        # Use the consumer object to initialize the client object
        client = oauth.Client(consumer)
         
        # Use your developer token and secret to instantiate access token object
        access_token = oauth.Token(
            key = self.user_token,
            secret = self.user_secret)
         
        client = oauth.Client(consumer, access_token)
        headers = {'x-li-format':'json'}
        reqDict = {}
        reqDict['client'] = client
        reqDict['headers'] = headers
        peopleList = self.companyIdGetter(reqDict)
        return peopleList

    def companyIdGetter(self, reqDict):
        print "[+] Scraping LinkedIn..."
        coIdList = []
        coIdDict = {}
        # Make call to LinkedIn to retrieve your own profile
        resp,content = reqDict['client'].request("http://api.linkedin.com/v1/companies?email-domain={0}".format(self.domain), "GET", "", reqDict['headers'])

        # this will be used to get the company id used for future requests
        count = 0
        json_entry = json.loads(content)
        if json_entry.has_key('errorCode'):
            print "[-] Error: Status %s - %s" % (json_entry['status'], json_entry['message'])
            sys.exit(0)
        else:
            baselen = len(json_entry['values'])
            while (count < baselen):
                coIdDict['id'] = count
                coIdDict['coId'] = json_entry['values'][count]['id']
                coIdDict['name'] = json_entry['values'][count]['name']
                coIdList.append(coIdDict.copy())
                count += 1  
          
            if len(coIdList) == 1:
                self.coId = [ coIdDict['coId']  ]
                print "[+] Only one coId found, using %s - %s" % (self.coId, coIdDict['name'])
            else:
                theader = PrettyTable([
                    "ID",
                    "coId",
                    "Company"
                    ])
                theader.align["ID"] = "l"
                theader.align["coId"] = "l"
                theader.align["Company"] = "l"
                theader.padding_width = 1  
                
                for item in coIdList:
                    theader.add_row([
                        "[" + str(item['id']) + "]",
                        item['coId'],
                        item['name']
                        ])
                
                print theader
                choice = -1
                while int(choice) >= len(coIdList) or int(choice) < 0:
                    choice = raw_input("Select the coId for scraping linkedin.com: ")
                    try:
                        choice = int(choice)
                        if choice < len(coIdList) and choice >= 0:
                            self.coId = [ c['coId'] for c in coIdList if c['id'] is choice ]
                            print "[+] Using %s" % (self.coId[0])
                    except:
                        print "[-] Invalid choice"
                        choice = -1 
            
            peopleList = self.companyPeopleGetter(reqDict)
            return peopleList

    def companyPeopleGetter(self, reqDict):
        fieldSelectors = "id,first-name,last-name,picture-url,headline,phone-numbers,im-accounts,twitter-accounts"
        # Pre-flight the request and get total returned results
        resp,content = reqDict['client'].request("http://api.linkedin.com/v1/people-search?facet=current-company,{0}".format(self.coId[0]), "GET", "", reqDict['headers'])
        json_entry = json.loads(content)
        if json_entry.has_key('errorCode'):
            print "[-] Error: Status %s - %s" % (json_entry['status'], json_entry['message'])
            sys.exit(0)
        else:
            paginateCeiling = json_entry['people']['_total']
            print "[+] %s employees found" % paginateCeiling
            if paginateCeiling > self.throttle:
                print "[-] Discovered employees greater than LinkedIn API throttle of %s" % self.throttle
                print "[+] Number of records scraped will be limited due to the throttle" 
            count = 0
            peopleList = []
            while (count < paginateCeiling):
                esp,content = reqDict['client'].request("http://api.linkedin.com/v1/people-search:(people:({0}))?facet=current-company,{1}&start={2}".format(fieldSelectors, self.coId[0], count), "GET", "", reqDict['headers'])
                json_entry = json.loads(content)
                if json_entry.has_key('people'):
                    if json_entry['people'].has_key('values'):
                        peopleList  += json_entry['people']['values']
                count += 10
            return peopleList   

    def formatResults(self, peopleList, emailformat):
        records = []
        for person in peopleList:
            record_dict = {}
            fname = person['firstName'].lower()
            lname = person['lastName'].lower()
            record_dict["Employee"]= person['firstName'] + " " + person['lastName']
            if person.has_key('headline'):
                record_dict["Role"] = person['headline']
            else:
                record_dict["Role"] = "Not Found"
            record_dict["Department"] = "Not Found"
            record_dict["Level"] = "Not Found"
            record_dict["City"] = "Not Found"
            record_dict["State"] = "Not Found"
            if emailformat[0] == "<first initial><last name>":
                record_dict["Email Address"] = fname[0] + lname + "@" + self.domain
            elif emailformat[0] == "<first initial>.<last name>":
                record_dict["Email Address"] = fname[0] + '.' + lname + "@" + self.domain
            elif emailformat[0] == "<first name><last name>":
                record_dict["Email Address"] = fname + lname + "@" + self.domain
            elif emailformat[0] == "<first name>.<last name>":
                record_dict["Email Address"] = fname + '.' + lname + "@" + self.domain
            elif emailformat[0] == "<first name>":
                record_dict["Email Address"] = fname + "@" + self.domain
            elif emailformat[0] == "<first name><last initial>":
                record_dict["Email Address"] = fname + lname[0] + "@" + self.domain
            elif emailformat[0] == "<first name>.<last initial>":
                record_dict["Email Address"] = fname + '.' + lname[0] + "@" + self.domain
            elif emailformat[0] == "<last name><first initial>":
                record_dict["Email Address"] = lname + fname[0] + "@" + self.domain
            elif emailformat[0] == "<last name>.<first initial>":
                record_dict["Email Address"] = lname + '.' + fname[0] + "@" + self.domain
            else:
                record_dict["Email Address"] = "Not Found"
            record_dict["Generic Phone"] = "Not Found"
            record_dict["Direct Phone"] = "Not Found"
            records.append(record_dict)

        if self.dbflag:
            write_dbfile(records, self.outfile, "linkedin.com") 
        else:
            write_outfile(records, self.outfile, "linkedin.com") 
