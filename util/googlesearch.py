#!/usr/bin/env python

import sys
import grequests 
import requests
import json
from prettytable import PrettyTable

class gSearch:
    def __init__(self, name, configDict, urls, proxies):
        self.glist = []
        self.comp_name = name
        self.cx_key = configDict['cx_key']
        self.api_key = configDict['api_key']
        self.urls = urls
        self.proxies = proxies
    
    def get_company(self):
        gdict = {}
        idx = 0
        co_id = self.comp_name
        cids = []
        for url in self.urls:
            gurl = ("https://www.googleapis.com/customsearch/v1?q=" + co_id + "&cx=" + \
                self.cx_key + \
                "&siteSearch=" + url + "&key=" + self.api_key \
                )
            headers = { \
            "Host": "www.googleapis.com", \
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0", \
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8", \
            "Accept-Language": "en-US,en;q=0.5", \
            "Accept-Encoding": "gzip, deflate", \
            "Connection": "keep-alive"
            }
            if len(self.proxies) == 0:
                gresponse = requests.get(gurl, headers=headers)
            else:
                requests.packages.urllib3.disable_warnings() #disable SSL warning when using proxies
                gresponse = requests.get(gurl, headers=headers, proxies=self.proxies, verify=False)
            data = json.loads(gresponse.text)
            if data.get('items'):
                for item in data['items']:
                    lnk = item['link']
                    if "connect.data.com" in url: #different criteria for data.com links
                        gdict['count'] = idx
                        gdict['link'] = lnk
                        gdict['company'] = item['title']
                        self.glist.append(gdict.copy())
                        idx += 1
                    else:
                        for x in item['pagemap']['metatags']:
                            gdict['count'] = idx
                            gdict['link'] = lnk
                            gdict['company'] = x['og:title']
                            if lnk[lnk.find("=")+1:len(lnk)] not in cids:
                                self.glist.append(gdict.copy())
                                idx += 1
                        cids.append(lnk[lnk.find("=")+1:len(lnk)])
            elif data.get('error'):
                sys.exit(data.get('error')['message'])
            else:
                print "[-] The web search returned no results for %s..." % (url)
        self.display_company_data()

    def display_company_data(self):
        if len(self.glist) is not None:
            print "\n[+] %d results found" % (len(self.glist))
            theader = PrettyTable(["Id", "Company", "Reference"])
            theader.align["Count"] = "l"
            theader.align["Company"] = "l"
            theader.align["Reference"] = "l"
            theader.padding_width = 1
            for result in self.glist:
                theader.add_row(["[" + str(result['count']) + "]", result['company'], result['link']])
            theader.add_row(["[" + str(len(self.glist)) + "]", "Enter your own URL", "Enter your own URL"])
            theader.add_row(["[" + str(len(self.glist)+1) + "]", "Search again", "Search again"])
            print theader
        else:
            print "\n[-] No results found\n"

